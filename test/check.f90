program check
use types, only: dp
use mvp_demo, only: integrate
use quadrature, only: gauss_pts, gauss_wts
use constants, only: pi
implicit none

integer :: i
integer, parameter :: Nq = 8
real(dp) :: xmin, xmax, r1, s
real(dp), allocatable :: xiq(:), wtq(:), x(:), y(:)
real(dp) :: jac, err
xmin = 0
xmax = pi

allocate(xiq(Nq), wtq(Nq), x(Nq), y(Nq))

call gauss_pts(Nq, xiq)
call gauss_wts(Nq, wtq)

jac = (xmax-xmin)/2

do i = 1, Nq
    x(i) = (xiq(i)+1)*jac + xmin
    wtq(i) = wtq(i) * jac
end do

do i = 1, Nq
    r1 = x(i)
    y(i) = sin(r1)**2
end do

s = integrate(wtq, y)
err = abs(s - pi/2)

print *, "integrate(sin(x)**2, (x, 0, pi)) = ", s
print *, "Exact answer is pi/2             = ", pi/2
print *, "Error                            = ", err

if (err > 1e-9) then
    error stop "Error too large"
end if

print *, "Test passed"

end program check
