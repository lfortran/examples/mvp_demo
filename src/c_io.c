#include <stdio.h>
#include <stdlib.h>

long long open(char *filename, char *mode)
{
   FILE *f;
   f = fopen(filename, mode);
   if (f == NULL) {
       printf("ERROR: fopen(\"%s\", \"%s\") failed\n", filename, mode);
       exit(1);
   }
   return (long long)f;
}

void close(long long h)
{
   FILE *f = (FILE*)h;
   fclose(f);
}

void write_i(long long h, int num)
{
   FILE *f = (FILE*)h;
   fprintf(f, "%d\n", num);
}

void write_rr(long long h, double r1, double r2)
{
   FILE *f = (FILE*)h;
   fprintf(f, "%23.17e %23.17e\n", r1, r2);
}

void read_i(long long h, int *num)
{
   FILE *f = (FILE*)h;
   fscanf(f, "%d\n", num);
}
