module types
use iso_fortran_env, only: int64
implicit none
private
public :: sp, dp, int64

integer, parameter :: sp = kind(0.0)
integer, parameter :: dp = kind(0.d0)

end module
