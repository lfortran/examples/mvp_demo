program sin_benchmark
use, intrinsic :: iso_fortran_env, only: dp => real64
implicit none

real(dp), parameter :: pi = 3.1415926535897932384626433832795_dp
integer, parameter :: N = 100000000
real(dp) :: x, r1, r2, err, r, i0
integer :: i

print *, "Executing sin_benchmark"

x = 10.5_dp
r1 = fast_sin(x)
r2 = -0.87969575997167_dp
err = abs(r1-r2)
print *, "x, fast_sin, reference"
print *, x, r1, r2
print *, "Error =", err
if (err > 1e-15_dp) error stop

print *, "Running benchmark..."
r = 0
do i = 1, N
    ! TODO: Workaround for https://gitlab.com/lfortran/lfortran/-/issues/573
    i0 = real(i,dp)
    r = r + fast_sin(i0)
end do
print *, "Done"
print *, r

contains

elemental real(dp) function fast_sin(x) result(r)
real(dp), intent(in) :: x
real(dp) :: y
y = modulo(x, 2*pi)
y = min(y, pi - y)
y = max(y, -pi - y)
y = min(y, pi - y)
r = kernel_dsin(y)
end function

! Accurate on [-pi/2,pi/2] to about 1e-16
elemental real(dp) function kernel_dsin(x) result(res)
real(dp), intent(in) :: x
real(dp), parameter :: S1 = 0.9999999999999990771_dp
real(dp), parameter :: S2 = -0.16666666666664811048_dp
real(dp), parameter :: S3 = 8.333333333226519387e-3_dp
real(dp), parameter :: S4 = -1.9841269813888534497e-4_dp
real(dp), parameter :: S5 = 2.7557315514280769795e-6_dp
real(dp), parameter :: S6 = -2.5051823583393710429e-8_dp
real(dp), parameter :: S7 = 1.6046585911173017112e-10_dp
real(dp), parameter :: S8 = -7.3572396558796051923e-13_dp
real(dp) :: z
z = x*x
res = x * (S1+z*(S2+z*(S3+z*(S4+z*(S5+z*(S6+z*(S7+z*S8)))))))
end function


! These intrinsics are not implemented yet
elemental real(dp) function min(x, y) result(r)
real(dp), intent(in) :: x, y
if (x < y) then
    r = x
else
    r = y
end if
end function

elemental real(dp) function max(x, y) result(r)
real(dp), intent(in) :: x, y
if (x > y) then
    r = x
else
    r = y
end if
end function

end program
